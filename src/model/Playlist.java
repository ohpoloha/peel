package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brucechen on 3/16/18.
 */
public class Playlist {
    private List<String> prerollVideos = new ArrayList<>();
    private List<String> contentVideos = new ArrayList<>();

    public List<String> getPlaylist() {
        List<String> playlist = new ArrayList<>();

        playlist.addAll(prerollVideos);
        playlist.addAll(contentVideos);
        return playlist;
    }

    public void addPrerollVideo(Video video) {
        prerollVideos.add(video.name);
    }

    public void addContentVideo(Video video) {
        contentVideos.add(video.name);
    }

    public void removePrerollVideo(Video video) {
        prerollVideos.remove(video.name);
    }

    public void removeContentVideo(Video video) {
        contentVideos.remove(video.name);
    }

    public void clearPreroll() {
        prerollVideos.clear();
    }

    public void clearContent() {
        contentVideos.clear();
    }
}
