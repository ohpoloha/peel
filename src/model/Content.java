package model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brucechen on 3/16/18.
 */
public class Content {
    public final String name;
    public final List<Video> videos;
    public final List<PreRoll> preroll;

    public Content(String name, List<Video> videos, List<PreRoll> preroll) {
        this.name = name;
        this.videos = videos;
        this.preroll = preroll;
    }
}
