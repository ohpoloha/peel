package model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brucechen on 3/16/18.
 */
public class PreRoll {
    public final String name;
    public final List<Video> videos;

    public PreRoll(String name, List<Video> videos) {
        this.name = name;
        this.videos = videos;
    }
}
