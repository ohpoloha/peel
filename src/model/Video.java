package model;

import java.util.List;

/**
 * Created by brucechen on 3/16/18.
 */
public class Video {
    public final String name;
    public final Attribute attributes;

    public Video(String name, Attribute attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public class Attribute {
        public final List<String> countries;
        public final String language;
        public final String aspect;

        public Attribute(List<String> countries, String language, String aspect) {
            this.countries = countries;
            this.language = language;
            this.aspect = aspect;
        }
    }
}
