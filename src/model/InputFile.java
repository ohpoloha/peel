package model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by brucechen on 3/16/18.
 */
public class InputFile {
    public final List<Content> content;
    public final List<PreRoll> preroll;

    public InputFile(List<Content> content, List<PreRoll> preroll) {
        this.content = content;
        this.preroll = preroll;
    }
}
