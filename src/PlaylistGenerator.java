import model.Content;
import model.Playlist;
import model.PreRoll;
import model.Video;

import java.util.*;

/**
 * Created by brucechen on 3/16/18.
 */
public class PlaylistGenerator {
    private HashMap<String, Content> hmContents = new HashMap<>();
    private List<Video> prerollVideos = new ArrayList<>();

    class Result {
        String message;
        List<Playlist> playlists = new ArrayList<>();
    }

    public PlaylistGenerator(List<Content> contentList, List<PreRoll> prerollList) {
        for (Content content : contentList) {
            hmContents.put(content.name, content);
        }

        for (PreRoll preroll : prerollList) {
            if (preroll.videos != null) {
                prerollVideos.addAll(preroll.videos);
            }
        }
    }

    public Result getPlaylist(String contentName, String country) {
        Content content = hmContents.get(contentName);
        Result result = new Result();

        if (content != null) {
            for (Video video : content.videos) {
                // loop through videos in content to find the ones that match the country
                if (video.attributes.countries.contains(country)) {
                    for (Video prerollVideo : prerollVideos) {
                        if (!prerollVideo.attributes.aspect.equals(video.attributes.aspect)) {
                            result.message = String.format(
                                    "(No legal playlist possible because the Pre-Roll Video isn't compatible with the aspect of the Content Video for the %s)", country);
                        } else if (!prerollVideo.attributes.language.equals(video.attributes.language)) {
                            result.message = String.format(
                                    "(No legal playlist possible because the Pre-Roll Video isn't compatible with the language of the Content Video for the %s)", country);
                        } else if (!prerollVideo.attributes.countries.contains(country)) {
                            result.message = String.format(
                                    "(No legal playlist possible because the Pre-Roll Video isn't compatible with the country of the Content Video for the %s)", country);
                        } else {
                            Playlist playlist = new Playlist();
                            playlist.addContentVideo(video);
                            playlist.addPrerollVideo(prerollVideo);
                            result.playlists.add(playlist);
                        }
                    }
                }
            }
        } else {
            result.message = "no content video found that matches the name";
        }

        return result;
    }
}
