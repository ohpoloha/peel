import com.google.gson.Gson;
import model.InputFile;
import model.Playlist;

import java.util.List;

public class Main {
    private static final String DATA = "{" +
            "\"content\": [" +
            "{" +
            "\"name\": \"MI3\"," +
            "\"preroll\": [{ \"name\": \"WB1\" }]," +
            "\"videos\": [" +
            "{ \"name\": \"V1\", \"attributes\": {\"countries\": [\"US\", \"CA\"], \"language\":" +
            "\"English\", \"aspect\": \"16:9\"} }," +
            "{ \"name\": \"V2\", \"attributes\": {\"countries\": [\"UK\"], \"language\": \"English\"," +
            "\"aspect\": \"4:3\"} }," +
            "{ \"name\": \"V3\", \"attributes\": {\"countries\": [\"UK\"], \"language\": \"English\"," +
            "\"aspect\": \"16:9\"} }" +
            "]" +
            "}]," +
            "\"preroll\": [" +
            "{" +
            "\"name\": \"WB1\"," +
            "\"videos\": [" +
            "{ \"name\": \"V4\", \"attributes\": {\"countries\": [\"US\"], \"language\": \"English\"," +
            "\"aspect\": \"4:3\"} }," +
            "{ \"name\": \"V5\", \"attributes\": {\"countries\": [\"CA\"], \"language\": \"English\"," +
            "\"aspect\": \"16:9\"} }," +
            "{ \"name\": \"V6\", \"attributes\": {\"countries\": [\"UK\"], \"language\": \"English\"," +
            "\"aspect\": \"4:3\"} }," +
            "{ \"name\": \"V7\", \"attributes\": {\"countries\": [\"UK\"], \"language\": \"English\"," +
            "\"aspect\": \"16:9\"} }" +
            "]" +
            "}]" +
            "}";

    public static void main(String[] args) {
        InputFile inputFile = new Gson().fromJson(DATA, InputFile.class);

        PlaylistGenerator playlistGenerator = new PlaylistGenerator(inputFile.content, inputFile.preroll);

        PlaylistGenerator.Result test1 = playlistGenerator.getPlaylist("MI3", "US");
        PlaylistGenerator.Result test2 = playlistGenerator.getPlaylist("MI3", "CA");
        PlaylistGenerator.Result test3 = playlistGenerator.getPlaylist("MI3", "UK");

        System.out.println("printing playlist for test1");
        if (test1.playlists.isEmpty()) {
            System.out.println(test1.message);
        } else {
            printPlaylist(test1.playlists);
        }
        System.out.println("");

        System.out.println("printing playlist for test2");
        if (test2.playlists.isEmpty()) {
            System.out.println(test2.message);
        } else {
            printPlaylist(test2.playlists);
        }
        System.out.println("");

        System.out.println("printing playlist for test3");
        if (test3.playlists.isEmpty()) {
            System.out.println(test3.message);
        } else {
            printPlaylist(test3.playlists);
        }
        System.out.println("");
    }

    private static void printPlaylist(List<Playlist> playlists) {
        for (Playlist playlist : playlists) {
            System.out.println(playlist.getPlaylist());
        }
    }
}
